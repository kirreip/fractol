/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   comp2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/03 14:13:10 by pmartin           #+#    #+#             */
/*   Updated: 2015/11/03 14:15:57 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

t_comp		ft_mulcd(t_comp z1, double k)
{
	t_comp z;

	z.a = z1.a * k;
	z.b = z1.b * k;
	return (z);
}

t_comp		ft_sstcomp(t_comp z1, t_comp z2)
{
	t_comp z;

	z.a = z1.a - z2.a;
	z.b = z1.b - z2.b;
	return (z);
}

double		ft_abs(t_comp z1)
{
	return (sqrt(ft_sq(z1.a) + ft_sq(z1.b)));
}

t_comp		ft_initcomp(double a, double b)
{
	t_comp z;

	z.a = a;
	z.b = b;
	return (z);
}
