/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hook.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/14 22:15:13 by pmartin           #+#    #+#             */
/*   Updated: 2015/11/22 04:01:13 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		expose_hook2(t_var *var)
{
	if (var->fract == 7)
		makejuliaship4(var);
	if (var->fract == 8)
		makebarnsleyj2(var);
	if (var->fract == 10)
		makemagnet2(var);
	if (var->fract == 11)
		makemanowar2(var);
	if (var->fract == 12)
		makelambda2(var);
	if (var->fract == 13)
		makespider2(var);
	if (var->fract == 14)
		makewafer2(var);
	return (0);
}

int		expose_hook(t_var *var)
{
	mlx_destroy_image(var->mlx_ptr, var->img_ptr);
	var->img_ptr = mlx_new_image(var->mlx_ptr, var->sizex, var->sizey);
	var->data = mlx_get_data_addr(var->img_ptr, &var->bpp,
								&var->sizeline, &var->endian);
	mlx_clear_window(var->mlx_ptr, var->win_ptr);
	var->y = var->sizey / 2;
	var->imgx = 0;
	var->imgy = 0;
	if (var->iter <= 1)
		var->iter = 2;
	if (var->fract == 1)
		makemandel2(var);
	if (var->fract == 2)
		makejulia2(var);
	if (var->fract == 3)
		makeship2(var);
	if (var->fract == 4)
		makejuliaship2(var);
	if (var->fract == 5)
		makenewton2(var);
	if (var->fract > 5)
		expose_hook2(var);
	mlx_put_image_to_window(var->mlx_ptr, var->win_ptr, var->img_ptr, 0, 0);
	return (0);
}

int		ft_key_hook(int k, t_var *var)
{
	if (k == 53)
		exit(1);
	if (k == 46)
		var->mono = (var->mono == 1) ? 0 : 1;
	if (k == 11)
		var->block = (var->block == 1) ? 0 : 1;
	if (k == 83)
		var->color -= 10;
	if (k == 84)
		var->color += 10;
	if (k == 86)
		var->color -= 100;
	if (k == 87)
		var->color += 100;
	if (k == 89)
		var->color -= 10000;
	if (k == 91)
		var->color += 10000;
	if (k == 69 || k == 78)
		var->zoomj = (k == 69) ? var->zoomj * 1.5 : var->zoomj / 1.5;
	if (k == 31 || k == 35)
		var->iter = (k == 35) ? var->iter * 1.5 : var->iter / 1.5;
	ft_arrowhook(k, var);
	expose_hook(var);
	return (0);
}

int		ft_motion_hook(int x, int y, t_var *var)
{
	if (var->block == 1)
	{
		var->aj = (double)(x - (var->sizex / 2)) / (double)var->zoomj;
		var->bj = (double)(y - (var->sizey / 2)) / (double)var->zoomj * -1;
		expose_hook(var);
	}
	expose_hook(var);
	return (0);
}

int		ft_mouse(int k, int x, int y, t_var *var)
{
	if (k == 5)
	{
		var->zoomj /= 1.5;
		var->mousx += (double)(x - (var->sizex / 2))
			/ ((double)var->zoomj * 1.5);
		var->mousy += (double)(y - (var->sizey / 2))
			/ ((double)var->zoomj * 1.5) * -1;
	}
	else if (k == 4)
	{
		var->zoomj *= 1.5;
		var->mousx -= (double)(x - (var->sizex / 2))
			/ ((double)var->zoomj * 1.5);
		var->mousy -= (double)(y - (var->sizey / 2))
			/ ((double)var->zoomj * 1.5) * -1;
	}
	expose_hook(var);
	return (0);
}
