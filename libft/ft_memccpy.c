/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 23:21:04 by pmartin           #+#    #+#             */
/*   Updated: 2014/11/08 03:01:26 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void *ft_memccpy(void *s1, const void *s2, int c, size_t n)
{
	char		*s1t;
	const char	*s2t;

	s1t = s1;
	s2t = s2;
	while (n--)
		if ((*s1t++ = *s2t++) == (char)c)
			return (s1t);
	return (0);
}
