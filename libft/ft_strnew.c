/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/09 00:47:18 by pmartin           #+#    #+#             */
/*   Updated: 2014/11/12 07:04:42 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char *ft_strnew(size_t size)
{
	char	*s;
	size_t	i;

	i = 0;
	if (size <= 0)
		return (0);
	if (!(s = (char *)malloc(sizeof(char) * (size + 1))))
		return (0);
	while (i < size)
		s[i++] = '\0';
	return (s);
}
