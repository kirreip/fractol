/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 02:07:54 by pmartin           #+#    #+#             */
/*   Updated: 2014/11/08 03:03:19 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	const char *temp;

	temp = s + ft_strlen(s);
	while (*temp != (char)c && temp != s)
		temp--;
	if (*temp != (char)c)
		return (0);
	return ((char *)temp);
}
