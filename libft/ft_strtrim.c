/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/20 00:34:17 by pmartin           #+#    #+#             */
/*   Updated: 2014/11/23 22:06:10 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int ft_spc(char c)
{
	return ((c == '\t' || c == '\n' || c == ' ') ? 1 : 0);
}

char *ft_strtrim(const char *s)
{
	char	*str;
	int		j;
	int		i;

	i = 0;
	j = ft_strlen(s) - 1;
	if (!s)
		return (0);
	if (ft_spc(s[0]) == 0 && ft_spc(s[j]) == 0)
		return (ft_strdup(s));
	while (ft_spc(s[i]) || ft_spc(s[j]))
		(ft_spc(s[i])) ? i++ : j--;
	if (!(str = ft_strnew(j - i + 2)))
		return (0);
	return (ft_strncpy(str, &s[i], j - i + 1));
}
