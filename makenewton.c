/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   makenewton.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/18 22:21:55 by pmartin           #+#    #+#             */
/*   Updated: 2015/11/22 03:56:24 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void		ft_pixel_put(t_var *var, int color)
{
	if (color == 0)
		return ;
	if (color == 1)
		color = mlx_get_color_value(var->mlx_ptr, 0x0000FF);
	if (color == 2)
		color = mlx_get_color_value(var->mlx_ptr, 0x00FF00);
	if (color == 3)
		color = mlx_get_color_value(var->mlx_ptr, 0xFF0000);
	ft_memcpy(var->data + (var->imgx * var->bpp / 8) + (var->imgy
														* var->sizeline),
			&color, 3);
}

double			ft_cmp(t_comp z1, t_comp z2)
{
	return (sqrt(ft_sq(z1.a - z2.a) + ft_sq(z1.b - z2.b)));
}

static int		ft_check(t_var *var)
{
	t_comp	z[4];
	t_comp	z1;
	double	tol;
	int		iter;

	tol = 0.01;
	z1.a = (double)var->x / var->zoomj + var->mousx;
	z1.b = (double)var->y / var->zoomj - var->mousy;
	z[1] = ft_initcomp(1, 0);
	z[2] = ft_initcomp(-0.5, sqrt(3) / 2);
	z[3] = ft_initcomp(-0.5, sqrt(3) / -2);
	iter = 0;
	while (ft_abs(ft_sstcomp(z1, z[1])) >= tol && ft_abs(ft_sstcomp(z1
		, z[2])) >= tol && ft_abs(ft_sstcomp(z1, z[3])) >= tol &&
		iter++ < var->iter)
		if (ft_abs(z1) > 0)
			z1 = ft_sstcomp(z1, ft_divcomp(ft_sstcomp(ft_mulcomp(ft_mulcomp(
			z1, z1), z1), z[1]), ft_mulcd(ft_mulcomp(z1, z1), 3)));
	if (ft_cmp(z1, z[1]) <= tol)
		return (1);
	if (ft_cmp(z1, z[2]) <= tol)
		return (2);
	if (ft_cmp(z1, z[3]) <= tol)
		return (3);
	return (0);
}

void			makenewton2(t_var *var)
{
	int x;
	int y;

	x = var->sizex / 3;
	y = var->sizey / -2;
	while (var->y != y)
	{
		var->x = var->sizex / -3 * 2;
		var->imgx = 0;
		while (var->x != x)
		{
			ft_pixel_put(var, ft_check(var));
			var->x++;
			var->imgx++;
		}
		var->imgy++;
		var->y--;
	}
}

void			makenewton(t_var *var)
{
	makenewton2(var);
	var->block = 0;
	mlx_put_image_to_window(var->mlx_ptr, var->win_ptr, var->img_ptr, 0, 0);
	mlx_expose_hook(var->win_ptr, expose_hook, var);
	mlx_mouse_hook(var->win_ptr, ft_mouse, var);
	mlx_hook(var->win_ptr, MOTION_NOTIFY, PTR_MOTION_MASK, ft_motion_hook, var);
	mlx_hook(var->win_ptr, KEY_PRESS, KEY_PRESS_MASK, ft_key_hook, var);
	mlx_loop(var->mlx_ptr);
}
