/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   makeship.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/02 19:30:51 by pmartin           #+#    #+#             */
/*   Updated: 2016/09/13 21:31:59 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void		ft_pixel_put(t_var *var, int color)
{
	if (color != 0 && var->mono == 1)
		color = var->color;
	if (color == 0)
		color = mlx_get_color_value(var->mlx_ptr, 0xFFFFFF);
	ft_memcpy(var->data + (var->imgx * var->bpp / 8)
			+ (var->imgy * var->sizeline), &color, 3);
}

static int		ft_check(t_var *var)
{
	t_comp	z;
	t_comp	tz;
	int		iter;

	iter = 0;
	z.a = 0;
	z.b = 0;
	while (iter++ < var->iter + 1)
	{
		if (z.a < 0)
			z.a *= -1;
		if (z.b < 0)
			z.b *= -1;
		tz = ft_mulcomp(z, z);
		z.a = tz.a + (double)var->x / var->zoomj + var->mousx;
		z.b = tz.b + (double)var->y / var->zoomj + var->mousy;
		if (z.a * z.a + z.b * z.b >= 4)
			return (iter * var->color);
	}
	return (0);
}

void			makeship2(t_var *var)
{
	int x;
	int y;

	var->y = var->sizey / -2;
	x = var->sizex / 3;
	y = var->sizey / 2;
	while (var->y != y)
	{
		var->x = var->sizex / -3 * 2;
		var->imgx = 0;
		while (var->x != x)
		{
			ft_pixel_put(var, ft_check(var));
			var->x++;
			var->imgx++;
		}
		var->imgy++;
		var->y++;
	}
}

void			makeship(t_var *var)
{
	var->color = 11;
	makeship2(var);
	mlx_put_image_to_window(var->mlx_ptr, var->win_ptr, var->img_ptr, 0, 0);
	mlx_expose_hook(var->win_ptr, expose_hook, var);
	mlx_mouse_hook(var->win_ptr, ft_mouse, var);
	mlx_hook(var->win_ptr, MOTION_NOTIFY, PTR_MOTION_MASK, ft_motion_hook, var);
	mlx_hook(var->win_ptr, KEY_PRESS, KEY_PRESS_MASK, ft_key_hook, var);
	mlx_loop(var->mlx_ptr);
}
