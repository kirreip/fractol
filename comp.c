/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   comp.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/21 04:03:07 by pmartin           #+#    #+#             */
/*   Updated: 2015/11/03 14:14:14 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

double		ft_sq(double k)
{
	return (k * k);
}

t_comp		ft_divcomp(t_comp z1, t_comp z2)
{
	t_comp z;

	z.a = (z1.a * z2.a + z1.b * z2.b) / (ft_sq(z2.a) + ft_sq(z2.b));
	z.b = (z2.a * z1.b - z1.a * z2.b) / (ft_sq(z2.a) + ft_sq(z2.b));
	return (z);
}

t_comp		ft_mulcomp(t_comp z1, t_comp z2)
{
	t_comp z;

	z.a = z1.a * z2.a - z1.b * z2.b;
	z.b = z2.a * z1.b + z1.a * z2.b;
	return (z);
}

t_comp		ft_addcomp(t_comp z1, t_comp z2)
{
	t_comp z;

	z.a = z1.a + z2.a;
	z.b = z1.b + z2.b;
	return (z);
}
