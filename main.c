/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/30 02:43:14 by pmartin           #+#    #+#             */
/*   Updated: 2015/11/18 18:43:56 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	var_init3(t_var *var)
{
	if (var->fract == 10)
		makemagnet(var);
	if (var->fract == 11)
		makemanowar(var);
	if (var->fract == 12)
		makelambda(var);
	if (var->fract == 13)
		makespider(var);
	if (var->fract == 14)
		makewafer(var);
}

void	var_init2(t_var *var)
{
	if (var->fract == 1)
		makemandel(var);
	if (var->fract == 2)
		makejulia(var);
	if (var->fract == 3)
		makeship(var);
	if (var->fract == 4)
		makejuliaship(var);
	if (var->fract == 5)
		makenewton(var);
	if (var->fract == 7)
		makejuliaship3(var);
	if (var->fract == 8)
		makebarnsleyj(var);
	if (var->fract > 9)
		var_init3(var);
}

void	var_init(t_var *var, int fract)
{
	var->sizex = 1001;
	var->sizey = 1001;
	var->mlx_ptr = mlx_init();
	var->img_ptr = mlx_new_image(var->mlx_ptr, var->sizex, var->sizey);
	var->data = mlx_get_data_addr(var->img_ptr, &var->bpp,
								&var->sizeline, &var->endian);
	var->win_ptr = mlx_new_window(var->mlx_ptr, var->sizex, var->sizey,
								"FRACTOL");
	var->y = var->sizey / 2;
	var->imgx = 0;
	var->imgy = 0;
	var->zoomj = 300;
	var->block = 1;
	var->mono = 0;
	var->aj = -1.076;
	var->bj = -0.00789;
	var->mousx = 0;
	var->mousy = 0;
	var->fract = fract;
	var_init2(var);
}

void	main2(char **av, t_var *var)
{
	if (ft_strcmp(av[1], "Juliaship2") == 0)
		var_init(var, (var->fract = 7));
	else if (ft_strcmp(av[1], "Barnsleyj") == 0)
		var_init(var, (var->fract = 8));
	else if (ft_strcmp(av[1], "Barnsleym") == 0)
		var_init(var, (var->fract = 9));
	else if (ft_strcmp(av[1], "Magnet") == 0)
		var_init(var, (var->fract = 10));
	else if (ft_strcmp(av[1], "Manowar") == 0)
		var_init(var, (var->fract = 11));
	else if (ft_strcmp(av[1], "Lambda") == 0)
		var_init(var, (var->fract = 12));
	else if (ft_strcmp(av[1], "Spider") == 0)
		var_init(var, (var->fract = 13));
	else if (ft_strcmp(av[1], "Wafer") == 0)
		var_init(var, (var->fract = 14));
	else
		osef();
}

int		main(int ac, char **av)
{
	t_var var;

	var.iter = 100;
	if (ac > 1)
	{
		if (ac == 3)
			if ((var.iter = ft_atoi(av[2])) > 0)
				;
		if (ft_strcmp(av[1], "Mandelbrot") == 0)
			var_init(&var, (var.fract = 1));
		else if (ft_strcmp(av[1], "Julia") == 0)
			var_init(&var, (var.fract = 2));
		else if (ft_strcmp(av[1], "Ship") == 0)
			var_init(&var, (var.fract = 3));
		else if (ft_strcmp(av[1], "Juliaship") == 0)
			var_init(&var, (var.fract = 4));
		else if (ft_strcmp(av[1], "Newton") == 0)
			var_init(&var, (var.fract = 5));
		else if (ft_strcmp(av[1], "Pheonix") == 0)
			var_init(&var, (var.fract = 6));
		else
			main2(av, &var);
	}
	return (0);
}
