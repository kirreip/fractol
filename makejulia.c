/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   makejulia.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/30 02:51:40 by pmartin           #+#    #+#             */
/*   Updated: 2015/11/22 02:51:59 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void		ft_pixel_put(t_var *var, int color)
{
	if (color == 0)
		color = mlx_get_color_value(var->mlx_ptr, 0x000000);
	if (var->mono == 1 && color != 0)
		color = var->color;
	ft_memcpy(var->data + (var->imgx * var->bpp / 8)
			+ (var->imgy * var->sizeline), &color, 3);
}

static int		ft_check(t_var *var)
{
	long double a;
	long double b;
	long double ta;
	long double tb;
	int			iter;

	iter = 0;
	a = (double)var->x / var->zoomj + var->mousx;
	b = (double)var->y / var->zoomj * 1.5 - var->mousy;
	while (iter++ < var->iter + 1)
	{
		ta = a * a - b * b;
		tb = 2 * a * b;
		a = ta + var->aj;
		b = tb + var->bj;
		if (sqrt(a * a + b * b) >= 2)
			return (iter * var->color);
	}
	return (0);
}

void			makejulia2(t_var *var)
{
	int x;
	int y;

	x = var->sizex / 2;
	y = var->sizey / -2;
	while (var->y != y)
	{
		var->x = var->sizex / -4 * 2;
		var->imgx = 0;
		while (var->x != x)
		{
			ft_pixel_put(var, ft_check(var));
			var->x++;
			var->imgx++;
		}
		var->imgy++;
		var->y--;
	}
}

void			makejulia(t_var *var)
{
	makejulia2(var);
	var->color = -127233;
	mlx_put_image_to_window(var->mlx_ptr, var->win_ptr, var->img_ptr, 0, 0);
	mlx_expose_hook(var->win_ptr, expose_hook, var);
	mlx_mouse_hook(var->win_ptr, ft_mouse, var);
	mlx_hook(var->win_ptr, MOTION_NOTIFY, PTR_MOTION_MASK, ft_motion_hook, var);
	mlx_hook(var->win_ptr, KEY_PRESS, KEY_PRESS_MASK, ft_key_hook, var);
	mlx_loop(var->mlx_ptr);
}
