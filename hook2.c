/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hook2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/22 01:13:51 by pmartin           #+#    #+#             */
/*   Updated: 2015/11/22 03:43:29 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		ft_arrowhook(int k, t_var *var)
{
	double o;
	double j;

	o = var->zoomj;
	j = 550;
	if (k == 123)
		(var->mousx) += j / o;
	if (k == 124)
		(var->mousx) -= j / o;
	if (k == 125)
		(var->mousy) -= j / o;
	if (k == 126)
		(var->mousy) += j / o;
	return (0);
}
