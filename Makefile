#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/05/11 18:23:17 by pmartin           #+#    #+#              #
#    Updated: 2015/11/22 01:34:04 by pmartin          ###   ########.fr        #
#                                                                              #
#******************************************************************************#


NAME = fractol
CC = cc
FLAGS = -Wall -Wextra -Werror
IDIR = includes/
LMLX = -lmlx -framework OpenGL -framework AppKit
LIBFT = libft/
SRC = main.c \
	makemandel.c \
	makejulia.c \
	makeship.c \
	makenewton.c \
	makejuliaship.c \
	makejuliaship2.c \
	makebarnsleyj.c \
	makemagnet.c \
	makemanowarmod.c \
	makelambda.c \
	makespider.c \
	makewafer.c \
	hook.c \
	hook2.c \
	comp.c \
	comp2.c \
	error.c
OBJ = $(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
	@make -C $(LIBFT) re
	@$(CC) -o $@ $^ $(FLAGS) $(LIBFT)libft.a $(LMLX) -I $(IDIR)

%.o: %.c
	@$(CC) $(FLAGS) -o $@ -c $< -I $(IDIR)

clean:
	@rm -f $(OBJ)

fclean: clean
	@rm -f $(NAME)
	@make -C $(LIBFT) fclean

re: fclean all

.PHONY: all $(NAME) clean fclean re