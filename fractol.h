/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/30 02:46:31 by pmartin           #+#    #+#             */
/*   Updated: 2015/12/02 23:13:20 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H
# include "libft/includes/libft.h"
# include <mlx.h>
# include <stdlib.h>
# include <fcntl.h>
# include <sys/types.h>
# include <math.h>
# include <float.h>
# define PTR_MOTION_MASK (1L<<6)
# define MOTION_NOTIFY 6
# define KEY_PRESS_MASK (1L<<0)
# define KEY_PRESS 3
typedef struct		s_var
{
	int				fract;
	int				sizex;
	int				sizey;
	int 			x;
	int				y;
	int				imgx;
	int				imgy;
	int				endian;
    int				sizeline;
    int				bpp;
    void			*img_ptr;
    void			*mlx_ptr;
    void			*win_ptr;
    char            *data;
	int				iter;
	int				color;
	double				zoomj;
	double			aj;
	double			bj;
	int				block;
	int				mono;
	double			mousx;
	double			mousy;
}					t_var;
typedef struct		s_comp
{
	double			a;
	double			b;
}					t_comp;
void		makemandel(t_var *var);
void		makemandel2(t_var *var);
void		makejulia(t_var *var);
void		makejulia2(t_var *var);
void		makeship(t_var *var);
void		makeship2(t_var *var);
void		makejuliaship(t_var *var);
void		makejuliaship2(t_var *var);
void		makejuliaship3(t_var *var);
void		makejuliaship4(t_var *var);
void		makenewton(t_var *var);
void		makenewton2(t_var *var);
void		makepheonix(t_var *var);
void		makepheonix2(t_var *var);
void		makebarnsleyj(t_var *var);
void		makebarnsleyj2(t_var *var);
void		makemagnet(t_var *var);
void		makemagnet2(t_var *var);
void		makemanowar(t_var *var);
void		makemanowar2(t_var *var);
void		makelambda(t_var *var);
void		makelambda2(t_var *var);
void		makespider(t_var *var);
void		makespider2(t_var *var);
void		makewafer(t_var *var);
void		makewafer2(t_var *var);
int			ft_mouse(int k, int x, int y, t_var *var);
int			ft_key_hook(int k, t_var * var);
int			ft_arrowhook(int k, t_var *var);
int			expose_hook(t_var *var);
int			ft_motion_hook(int x, int y, t_var *var);
double		ft_sq(double k);
t_comp		ft_mulcomp(t_comp z1, t_comp z2);
t_comp		ft_divcomp(t_comp z1, t_comp z2);
t_comp		ft_addcomp(t_comp z1, t_comp z2);
t_comp		ft_mulcd(t_comp z1, double k);
t_comp		ft_sstcomp(t_comp z1, t_comp z2);
t_comp		ft_initcomp(double a, double b);
double		ft_abs(t_comp z1);
void		osef();
#endif
