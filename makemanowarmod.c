/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   makemanowarmod.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/18 18:32:44 by pmartin           #+#    #+#             */
/*   Updated: 2016/09/14 15:48:26 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void		ft_pixel_put(t_var *var, int color)
{
	if (color != 0 && var->mono == 1)
		color = var->color;
	if (color == 0)
		color = mlx_get_color_value(var->mlx_ptr, 0xFFFFFF);
	ft_memcpy(var->data + (var->imgx * var->bpp / 8)
			+ (var->imgy * var->sizeline), &color, 3);
}

static int		ft_check(t_var *var)
{
	int		iter;
	t_comp	z[5];

	z[3] = ft_initcomp((double)var->x / var->zoomj + var->mousx,
					(double)var->y / var->zoomj - var->mousy);
	z[2] = z[3];
	z[0] = ft_initcomp(0, 0);
	iter = 0;
	while (iter++ < var->iter)
	{
		z[1] = z[0];
		z[0] = ft_addcomp(ft_addcomp(ft_mulcomp(z[0], z[0]), z[2]), z[3]);
		if ((z[0].a * z[0].a + z[0].b * z[0].b) > 16)
			return (iter * var->color);
		z[2] = z[1];
	}
	return (0);
}

void			makemanowar2(t_var *var)
{
	int x;
	int y;

	x = var->sizex / 2;
	y = var->sizey / -2;
	while (var->y != y)
	{
		var->x = var->sizex / -4 * 2;
		var->imgx = 0;
		while (var->x != x)
		{
			ft_pixel_put(var, ft_check(var));
			var->x++;
			var->imgx++;
		}
		var->imgy++;
		var->y--;
	}
}

void			makemanowar(t_var *var)
{
	var->color = -129000;
	var->aj = -1.48667;
	var->bj = -0.413333;
	var->block = 0;
	makemanowar2(var);
	mlx_put_image_to_window(var->mlx_ptr, var->win_ptr, var->img_ptr, 0, 0);
	mlx_expose_hook(var->win_ptr, expose_hook, var);
	mlx_mouse_hook(var->win_ptr, ft_mouse, var);
	mlx_hook(var->win_ptr, MOTION_NOTIFY, PTR_MOTION_MASK, ft_motion_hook, var);
	mlx_hook(var->win_ptr, KEY_PRESS, KEY_PRESS_MASK, ft_key_hook, var);
	mlx_loop(var->mlx_ptr);
}
