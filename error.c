/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/18 18:21:45 by pmartin           #+#    #+#             */
/*   Updated: 2015/11/18 18:41:12 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	osef(void)
{
	ft_putendl("Usage:");
	ft_putendl("./fractol 'Fractal' (iterations)");
	ft_putendl("Fractals:");
	ft_putendl("Mandelbrot");
	ft_putendl("Julia");
	ft_putendl("Juliaship");
	ft_putendl("Juliaship2");
	ft_putendl("Ship");
	ft_putendl("Magnet");
	ft_putendl("Manowar");
	ft_putendl("Lambda");
	ft_putendl("Newton");
	ft_putendl("Wafer");
	ft_putendl("Barnsleyj");
	ft_putendl("Spider");
	ft_putendl("Iterations is an integer");
}
